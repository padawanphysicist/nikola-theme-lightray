'use strict';
const upath = require('upath');

module.exports = function getPath(filePath) {
    return upath.resolve(upath.dirname(__filename), `../${filePath}`);
};
