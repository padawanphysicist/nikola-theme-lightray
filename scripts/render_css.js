'use strict';
const upath = require('upath');
const fs = require('fs');
const sass = require('sass');
const prettier = require('prettier');
const get_path = require('./get_path');

module.exports = function render_css(filePath) {    
    const fileName = upath.basename(filePath);
    const destPath = upath.changeExt(get_path(`dist/lightray/assets/css/${fileName}`), 'css');
    console.log(`# [INFO] Rendering ${filePath} to ${destPath}`);

    const res = sass.compile(filePath);
    const prettified = prettier.format(res.css, {
        printWidth: 1000,
        tabWidth: 4,
        singleQuote: true,
        proseWrap: 'preserve',
        endOfLine: 'lf',
        parser: 'css',
        htmlWhitespaceSensitivity: 'ignore'
    });

    prettified.then(function (res) {
        fs.writeFileSync(destPath, res);
    });
};
