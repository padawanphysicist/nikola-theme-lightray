'use strict';
const { globSync } = require('glob')
const render_css = require('./render_css');

const sassfiles = globSync('**/*.scss', { ignore: 'node_modules/**' })

sassfiles.map(filePath => render_css(filePath));
// render_css(filePath);
