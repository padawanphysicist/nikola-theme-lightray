'use strict';
const upath = require('upath');
const fs = require('fs');

// Root directory for the production code
const dir = "dist/lightray";

["css", "js"].map(i => `${dir}/assets/${i}`).map(d => !fs.existsSync(d) && fs.mkdirSync(d, { recursive: true }));


