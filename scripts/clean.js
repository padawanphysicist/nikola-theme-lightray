'use strict';
const upath = require('upath');
const fs = require('fs');

const dist_dir = upath.resolve(upath.dirname(__filename), '../dist');

fs.rm(dist_dir, { recursive: true, force: true }, err => {
    if (err) throw err;
    console.log(`# [INFO] ${dist_dir} is deleted!`);
});
