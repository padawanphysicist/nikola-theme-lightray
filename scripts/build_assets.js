'use strict';
const fs = require('fs');
const { globSync } = require('glob')
const path = require('path');

[].concat(
    globSync('./node_modules/bootstrap-icons/font/fonts/*'),
    globSync('./src/fonts/**/*.ttf')
).map(f => fs.cp(f, `./dist/lightray/assets/css/fonts/${path.basename(f)}`, {recursive: true}, (err) => {}));

fs.cp('./src/templates', './dist/lightray/templates', {recursive: true}, (err) => {/* callback */});
fs.cp('./src/lightray.theme', './dist/lightray/lightray.theme', {recursive: true}, (err) => {/* callback */});
fs.cp('./src/bundles', './dist/lightray/bundles', {recursive: true}, (err) => {/* callback */});
