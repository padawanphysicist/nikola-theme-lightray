'use strict';
const fs = require('fs');
const path = require('path');
const { globSync } = require('glob')

const jsfiles = [].concat(
    globSync('**/*.js', { ignore: ['node_modules/**', 'scripts/**', 'assets/**'] }),
    globSync('./node_modules/bootstrap/dist/js/bootstrap.bundle.min*'),
    globSync('./node_modules/jquery/dist/jquery.min*'),
    globSync('./node_modules/masonry-layout/dist/masonry.pkgd.min*'),
    globSync('./node_modules/imagesloaded/imagesloaded.pkgd.min.js*')
);

jsfiles.map(filePath => fs.copyFile(filePath, `./dist/lightray/assets/js/${path.basename(filePath)}`, (err) => {}));
