/////////////
// Edit Me //
/////////////

// Add edit buttons
$('*[data-edit-this]').each(function(){
    const git_org = 'padawanphysicist';
    const git_repo = 'padawanphysicist.gitlab.io';
    const branch = 'main';
    var filepath = $(this).data('edit-this');
	var edit_link = '<a class="d-none edit-this-btn position-absolute btn btn-default btn-circle btn-lg btn-primary" href="https://gitlab.com/'+git_org+'/'+git_repo+'/-/blob/'+branch+'/pages/'+filepath+'" title="Editar" target="_blank"><i class="bi bi-pencil"></i></a>';
	$(this).addClass('edit-this').append(edit_link);
});

// Toggle behaviour
$('#edit-tools-btn').on('click', function(event) {
    $('*[data-edit-this]').each(function(){
        $(this).toggleClass('edit-this-active');
    });
    $('.edit-this-btn').each(function() {
             $(this).toggleClass('d-none');
    });
});

/////////////////////////
// Art gallery masonry //
/////////////////////////
function populateArtwork(data) {
    function card(artist) {
        return([
            `<div class="col-sm-6 col-lg-4">`,
            `<div class="card grow">`,
            `<img src="${artist.img}" class="img-fluid">`,
            `<div class="card-body">`,
            `<span class="text-muted font-italic">${artist.name}</span></br>`,
            `<span class="card-text mt-2 artwork-description">`,
            `${artist.description}`,
            `</span>`,
            `</div>`,
            `</div>`,
            `</div>`
        ].join("\n"));
    }

    // Add to DOM
    $('#artwork').append(
        data.map(artist => {
            return(card(artist));
        }).join("\n")
    );
    // init Masonry
    var $influencias = $('#artwork').masonry({});
    // layout Masonry after each image loads
    $influencias.imagesLoaded().progress( function() {
      $influencias.masonry('layout');
    });
}
function populateInfluencias(data) {
    function card(artist) {
        return([
            `<div class="col-sm-6 col-lg-4">`,
            `<div class="card grow">`,
            `<img src="${artist.img}" class="img-fluid">`,
            `<div class="card-body">`,
            `<a href="${artist.url}" class="stretched-link text-muted fst-italic">${artist.name}</a>`,
            `<p class="card-text mt-5">`,
            `${artist.description}`,
            `</p>`,
            `</div>`,
            `</div>`,
            `</div>`
        ].join("\n"));
    }

    // Add to DOM
    $('#influencias').append(
        data.map(artist => {
            return(card(artist));
        }).join("\n")
    );
    // init Masonry
    var $influencias = $('#influencias').masonry({});
    // layout Masonry after each image loads
    $influencias.imagesLoaded().progress( function() {
      $influencias.masonry('layout');
    });
}

const influencias_grid = $.ajax({
    url : "/metadata-influencias.json",
    dataType: "json"}).done(populateInfluencias);
const artwork_grid = $.ajax({
    url : "/metadata-artwork.json",
    dataType: "json"}).done(populateArtwork);
